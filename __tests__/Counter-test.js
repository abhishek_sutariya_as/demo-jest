import React from 'react';
import renderer from 'react-test-renderer';
import Counter from '../src/Counter';
import {cleanup, render, fireEvent} from '@testing-library/react-native';

it('renders correctly', () => {
  const tree = renderer.create(<Counter />).toJSON();
  expect(tree).toMatchSnapshot();
  // let counterData = renderer.create(<Counter />).getInstance();
  // console.log('counterData', counterData);
  // expect(counterData.incrementCounter(2)).toEqual(3);
});
