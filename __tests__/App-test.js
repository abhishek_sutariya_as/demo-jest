/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';
import {cleanup, render, fireEvent} from '@testing-library/react-native';
import {toHaveStyle} from '@testing-library/jest-native';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

afterEach(cleanup);

describe('App test render', () => {
  it('should show hello world', () => {
    const helloWorldText = 'Hello World';
    const notFoundText = 'Not found text';
    const {toJSON, getByText, queryByText} = render(<App />);
    const foundHelloWorldTextElement = getByText(helloWorldText);
    const notFoundTextElement = queryByText(notFoundText);
    expect(foundHelloWorldTextElement.props.children).toEqual(helloWorldText);
    expect(notFoundTextElement).toBeNull();
    expect(toJSON()).toMatchSnapshot();
  });
  // If any module we want to mock then use below syntax
  jest.mock('react-native-video', () => 'Video');

  it('should find the button via testId', () => {
    const testIdName = 'pressMeButton';
    const {getByTestId} = render(<App />);
    const foundButton = getByTestId(testIdName);
    expect(foundButton).toBeTruthy();
  });

  it('should match the body style', () => {
    const {getByTestId} = render(<App />);
    const foundBodyElement = getByTestId('body');
    expect(foundBodyElement.props.style.backgroundColor).toEqual('#ffffff');
  });

  it('should match the sectionTitle style', () => {
    const {getByText} = render(<App />);
    const foundSectionTitle = getByText('Hello World');
    expect(foundSectionTitle).toHaveStyle({
      fontSize: 24,
      fontWeight: '600',
      color: '#000000',
    });
  });

  it('should not find the toggled component', () => {
    const hiddenToggledText = 'I am the toggled component';
    const {queryByText} = render(<App />);
    const notFoundHiddenComponent = queryByText(hiddenToggledText);
    expect(notFoundHiddenComponent).toBeFalsy();
  });

  it('should find the toggled component', () => {
    const testIdName = 'pressMeButton';
    const toggledText = 'I am the toggled component';
    const {getByTestId, getByText} = render(<App />);
    const foundButton = getByTestId(testIdName);
    fireEvent.press(foundButton);
    const foundToggledText = getByText(toggledText);
    expect(foundToggledText.props.children).toEqual(toggledText);
  });
});
