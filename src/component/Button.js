import React from 'react';
import {ActivityIndicator, TouchableOpacity} from 'react-native';

export default function Button(props) {
  const {label, onPress, loading, ...others} = props;
  return (
    <TouchableOpacity onPress={onPress} testID="button">
      {loading ? (
        <ActivityIndicator testID="button-loading" size={24} color={'white'} />
      ) : (
        <Text>{label}</Text>
      )}
    </TouchableOpacity>
  );
}
