import { fireEvent } from "@testing-library/react-native";
import React from "react";
import Button from "../component/Button";

describe('Button', () => {
    test('Should render correctly', () => {
        const wrapper = render(<Button label='' onPress={jest.fn()}/>);
        wrapper.getByTestId('button')
    })

    test('Should render loader when loading', () => {
        const wrapper = render(<Button label='' onPress={jest.fn()} loading/>);
        wrapper.getByTestId('button-loading')
    })

    test('Should call given onPress when clicked', () => {
        const mockOnPress = jest.fn()
        const wrapper = render(<Button label='' onPress={mockOnPress} loading/>);
        const button = wrapper.getByTestId('button')
        fireEvent.press(button)
        expect(mockOnPress).toHaveBeenCalled()
    })

    test('Should render label', () => {
        const wrapper = render(<Button label='mock-label' onPress={jest.fn()}/>);
        wrapper.getByTestId('mock-label')
    })

    test('Should accept custom view props', () => {
        const wrapper = render(<Button label='' onPress={jest.fn()} testId="mock-test-id"/>)
        wrapper.getByTestId('mock-test-id')
    })
})

//19th udemy lec example