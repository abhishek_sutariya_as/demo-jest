import React, {useState} from 'react';
import {Button, Text, View} from 'react-native';

const Counter = () => {
  const [counter, setCounter] = useState(0);

  const incrementCounter = () => {
    setCounter(prevCounter => prevCounter + 1);
    return counter;
  };

  const decrementCounter = () => {
    setCounter(prevCounter => prevCounter - 1);
  };

  return (
    <View
      style={{alignSelf: 'center', justifyContent: 'space-evenly', flex: 1}}>
      <Button title="increment" onPress={incrementCounter} />
      <Text>counter value = {counter}</Text>
      <Button title="decrement" onPress={decrementCounter} />
    </View>
  );
};

export default Counter;
